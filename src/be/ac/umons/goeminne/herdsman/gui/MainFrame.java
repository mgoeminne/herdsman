package be.ac.umons.goeminne.herdsman.gui;

import javax.swing.*;
import be.ac.umons.goeminne.herdsman.core.MySQLSession;
import be.ac.umons.goeminne.herdsman.plugin.Plugin;

public class MainFrame extends JFrame 
{
	private JTabbedPane pane;
	private MySQLSession session;
	
	public MainFrame(String title, MySQLSession session)
	{
		super(title);
		
		this.session = session;
		
		MainEH listener = new MainEH(this);
		
		JMenuBar bar = new JMenuBar();
			JMenu fileM = new JMenu("Fichier");
				JMenuItem quit = new JMenuItem("Quitter");
				quit.setActionCommand("QUIT");
				quit.addActionListener(listener);
				fileM.add(quit);
			JMenu bddM = new JMenu("Base de donn�es");
				JMenuItem param = new JMenuItem("Param�tres");
				param.setActionCommand("DATA_SOURCES");
				param.addActionListener(listener);
				bddM.add(param);
			JMenu tools 	= new JMenu("Outils");
				JMenuItem merge = new JMenuItem("Fusion d'identit�s");
				merge.setActionCommand("MERGING_IDENTITY");
				merge.addActionListener(listener);
				tools.add(merge);
				
			bar.add(fileM);
			bar.add(bddM);
			bar.add(tools);
			
		this.setJMenuBar(bar);
		
		pane = new JTabbedPane();
		
		this.add(pane);

		this.pack();	
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
	}
	
	/**
	 * Ajoute un plugin � la liste des plugins disponibles.
	 * @param p Le plugin � ajouter.
	 */
	public <E extends Number> void addPlugin(Plugin<E> p)
	{
		this.pane.add(p.getPanel(this), p.shortName());
	}
	
	public final MySQLSession getSession()
	{
		return this.session;
	}	
}
