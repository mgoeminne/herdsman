package be.ac.umons.goeminne.herdsman.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.*;

import net.miginfocom.swing.MigLayout;

public class DBChoiceFrame extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel p;
	private JTextField hostText, sourceText, mailText, bugtrackerText, userText;
	private JSpinner port;
	private JPasswordField pwd;
	
	public DBChoiceFrame(String title)
	{
		super(title);

		MigLayout layout = new MigLayout("wrap 2");
		this.setLayout(new BorderLayout());
		p = new JPanel();
		p.setLayout(layout);
		
		hostText	= new JTextField();
		port		= new JSpinner(new SpinnerNumberModel(8889, 1024, 65535, 1));
		userText	= new JTextField();
		pwd 		= new JPasswordField();
		sourceText 	= new JTextField();
		mailText 	= new JTextField();
		bugtrackerText = new JTextField();
		
		p.add(new JLabel("Host :"));
		p.add(hostText, "width 30:200:");
		
		p.add(new JLabel("Port :"));
		p.add(port, "width 30:200:");
		
		p.add(new JLabel("User name :"));
		p.add(userText, "width 30:200:");
		
		p.add(new JLabel("Password :"));
		p.add(pwd, "width 30:200:");
		
		p.add(new JLabel("Code source :"));
		p.add(sourceText, "width 30:200:");
		
		p.add(new JLabel("Mail :"));
		p.add(mailText, "width 30:200:");
		
		p.add(new JLabel("Bugtracker :"));
		p.add(bugtrackerText, "width 30:200:");
	
		
		
		JPanel choicePanel = new JPanel();
		choicePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		DBEH listener = new DBEH(this);
		
		JButton cancel 	= new JButton("Annuler");
			cancel.setActionCommand("CANCEL");
			cancel.addActionListener(listener);
			
		JButton ok		= new JButton("Confirmer");
			ok.setActionCommand("CONFIRM");
			ok.addActionListener(listener);
		choicePanel.add(cancel);
		choicePanel.add(ok);
		
		
		this.add(p, BorderLayout.CENTER);
		this.add(choicePanel, BorderLayout.SOUTH);
		
		
		this.pack();		
	}
}
