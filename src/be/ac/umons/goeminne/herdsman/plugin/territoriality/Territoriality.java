package be.ac.umons.goeminne.herdsman.plugin.territoriality;

import be.ac.umons.goeminne.herdsman.core.Couple;
import be.ac.umons.goeminne.herdsman.core.MySQLSession;
import be.ac.umons.goeminne.herdsman.plugin.IntegerPlugin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimePeriodValue;
import org.jfree.data.time.TimePeriodValues;
import org.jfree.data.time.TimePeriodValuesCollection;

public class Territoriality extends IntegerPlugin
{
	public Territoriality()
	{
		super();
	}
	
	/**
	 * On red�fini la m�thode parce qu'il faut tenir compte des effets cumulatifs de la m�trique.
	 */
	public ArrayList<Couple<Integer, Integer>> compute(MySQLSession session, int first, int last) throws SQLException
	{
		start();
		ArrayList<Couple<Integer, Integer>> ret = new ArrayList<Couple<Integer, Integer>>();
		// R�cup�ration de la liste des fichiers
		Hashtable<Integer, File> files = new Hashtable<Integer, File>();
		String query = 	"SELECT id, file_name " +
						"FROM files ";
		//System.out.println("plop");
		Statement sc = session.getSCStatement();
		ResultSet repF = sc.executeQuery(query);
		while(repF.next())
		{
			int id = repF.getInt("id");
			String name = repF.getString("file_name");
			File f = new File(id, name);
			files.put(id, f);
		}
		sc.close();
		
		// R�cup�ration des id int�ressants
		query = 	"SELECT rev " + 
						"FROM scmlog " + 
						"WHERE rev BETWEEN " + 1 + " AND " + last + " " +
						"ORDER BY id ASC";
		
		sc = session.getSCStatement();
		ResultSet repID = sc.executeQuery(query);
		
		while(repID.next())
		{
			int revision = repID.getInt("rev");
			
			String queryFile = 	"SELECT a.type AS type, a.file_id AS file, l.committer_id AS committer " + 
								"FROM actions AS a, scmlog AS l " + 
								"WHERE ( " +
											"(a.commit_id = " + revision + ") "+
											"AND " +
											"(a.commit_id = l.rev) " + 
								")";

			sc = session.getSCStatement();
			ResultSet repA = sc.executeQuery(queryFile);
			
			while(repA.next())
			{
				int file_id = repA.getInt("file");
				String type = repA.getString("type");
				int committer_id = repA.getInt("committer");
			
				if(type.equals("D"))
					files.get(file_id).delete(revision, committer_id);
				else
					files.get(file_id).touch(revision, committer_id);				
			}
			sc.close();
			
			// du nombre de fichiers territoriaux
			int count = 0;
			int alive = 0;
			
			for(File f : files.values())
			{
				if(f.isTerritorial())
				{
					assert(f.isAlive());
					count++;
				}
					
				if(f.isAlive())
					alive++;
			}
					
			assert(alive >= count) : "Alive (" + alive + ") < Terri (" + count + ")" ;
				
			if(revision >= first)
				ret.add(new Couple<Integer, Integer>(revision, count));
			
			
			setProgression((double)revision / (double)(last - 1));
		}
		files.clear();
		sc.close();
		
		finish();
		return ret;
	}
	
	/**
	 * Calcule la m�trique pour une r�vision donn�e. S'il y a des d�pendances entre les r�visions, c'est au plugin
	 * de faire tous les calculs n�cessaires pour calculer ces d�pendances. Ce n'est typiquement pas le cas : g�n�ralement la valeur d'une m�trique pour 
	 * une r�vision r ne d�pend pas de la valeur pour la r�vision r-1.
	 * @param session	La session MySQL.
	 * @param rev_id	L'id de la r�vision � consid�rer.
	 * @return	La valeur de la m�trique pour cette r�vision.
	 * @throws SQLException
	 */
	public Integer compute(MySQLSession session, int revision_id) throws SQLException
	{
		ArrayList<Couple<Integer, Integer>> l = compute(session, 1, revision_id);
		assert( l.get(l.size()-1).getFirst() == revision_id);
		
		return l.get(l.size()-1).getSecond();
	}	
	
	
	
	public String toString()
	{
		return "Territoriality";
	}
	
	public String getDescription()
	{
		return "Number of territorial files. A new file is territorial, its creator is its owner. If it is touched by somebody who is not its owner, it losts its territoriality. If it is touched 2 times by the same person, it becomes territorial again, the toucher becomes its owner.";
	}
}
