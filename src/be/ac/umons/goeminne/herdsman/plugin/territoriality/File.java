package be.ac.umons.goeminne.herdsman.plugin.territoriality;

public class File 
{
	private int id;				// L'identifiant du fichier
	private String name;		// Le nom du fichier (ie sa place dans le path)
	private Integer currentOwner;	// L'id du possesseur actuel du fichier
	private boolean territorial;	// true si le fichier est territorial
	private boolean bird;		// true si le fichier est n�
	private boolean dead;		// true si le fichier est mort
	private Integer firstRevision;		// La r�vision � laquelle le fichier est n�
	private Integer lastTouched;	// La r�vision � laquelle le fichier a �t� touch� pour la derni�re fois
	
	/**
	 * D�clare un fichier.
	 * @param id		L'identifiant du fichier
	 * @param name		Le nom du fichier
	 */
	public File(int id, String name)
	{
		this.id = id;
		this.name = name;
		this.currentOwner = null;
		this.territorial  = false;
		this.bird = false;
		this.dead = false;
		this.firstRevision = null;
		this.lastTouched = null;
	}
	
	/**
	 * Quelqu'un touche le fichier
	 * @param revision	La r�vision � laquelle le fichier est touch�.
	 * @param toucher	La personne qui touche le fichier�.
	 */
	public void touch(int revision, int toucher)
	{
		assert(!dead);
		
		if(this.bird)		// Le fichier existait d�j�
		{
			assert(isAlive());
			lastTouched = revision;
		}
		else		// C'est une naissance
		{
			assert(!isAlive());
		
			this.bird = true;
			currentOwner = toucher;
			territorial = true;
			firstRevision = revision;
			
			assert(isAlive());
		}
		
		if((currentOwner == null) || ((currentOwner != null) && (currentOwner == toucher)))
		{
			territorial = true;
			currentOwner = toucher;
		}
		else
		{
			territorial = false;
			currentOwner = null;
		}
	}
	
	
	/**
	 * Supprime le fichier.
	 * @param revision	La r�vision � laquelle le fichier dispara�t.
	 * @param toucher	L'id de la personne qui supprime le fichier.
	 */
	public void delete(int revision, int toucher)
	{
		// assert(this.bird);		// Ne fonctionne pas pour cause d'incoh�rence dans la BDD de flossmetric
		// assert(isAlive());		// Idem
		
		dead = true;
		currentOwner = null;
		territorial = false;
		lastTouched = revision;
		
		assert(!isAlive());
	}
	
	
	/**
	 * D�termine si le fichier est vivant ou pas.
	 * @return	true si le fichier est vivant, false sinon.
	 */
	public boolean isAlive()
	{
		return bird && !dead;
	}
	
	public boolean isTerritorial()
	{
		return this.territorial;
	}
	
	public int getID()
	{
		return this.id;
	}
	
	public Integer getFirstRevision()
	{
		return this.firstRevision;
	}
}
