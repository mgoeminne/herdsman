package be.ac.umons.goeminne.herdsman.plugin.territoriality;

import java.sql.SQLException;

import be.ac.umons.goeminne.herdsman.core.DateRangeType;
import be.ac.umons.goeminne.herdsman.core.DateTranslator;
import be.ac.umons.goeminne.herdsman.core.MySQLSession;
import be.ac.umons.goeminne.herdsman.core.Couple;
import be.ac.umons.goeminne.herdsman.core.SourceDateTranslator;
import be.ac.umons.goeminne.herdsman.plugin.IntegerPlugin;

import java.util.*;

import org.jfree.data.time.DateRange;
import org.jfree.data.time.Day;

public class Test 
{

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException 
	{
		MySQLSession session = new MySQLSession("root", "root", "Evince");
		session.setSourceCodeDB("evincefm");
		
		IntegerPlugin t = new Territoriality();
		DateTranslator dt = new SourceDateTranslator(session);
		System.out.println("a");
		ArrayList<Couple<DateRange, Integer>> ret = dt.convertToDate(t.compute(session), true, DateRangeType.MONTH, 5);
		System.out.println("b " + ret.size());
		
		for(Couple<DateRange, Integer> c : ret)
		{
			DateRange dr = c.getFirst();
			Integer val = c.getSecond();
			
			System.out.println(dr.getLowerDate() + "\t" + dr.getUpperDate() + "\t : " + val);
		}
		
		session.close();
	}
}
