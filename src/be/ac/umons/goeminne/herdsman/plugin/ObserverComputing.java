package be.ac.umons.goeminne.herdsman.plugin;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JProgressBar;

import be.ac.umons.goeminne.herdsman.core.Couple;

public class ObserverComputing<E> extends Thread 
{
	private JProgressBar progress;		// La barre de progression � mettre � jour
	private ComputingThread<E> thread;		// Le thread � surveiller
	private ComputeEventListener listener;
	private static final int SLEEP_TIME = 100;
	
	/**
	 * 
	 * @param listener	Un �couteur des �v�nements relatifs aux calculs.
	 * @param progress	La barre de progression � mettre � jour.
	 * @param thread	Le thread � lancer.
	 */
	public ObserverComputing(ComputeEventListener listener, JProgressBar progress, ComputingThread<E> thread)
	{
		this.progress = progress;
		this.progress.setMinimum(0);
		this.progress.setMaximum(1000);
		this.thread = thread;
		this.listener = listener;
	}
	
	public void run()
	{
		this.progress.setValue(0);
		this.thread.start();
		
		System.out.println(this.thread.isFinished());
		while(!this.thread.isFinished())
		{
			try 
			{
				Thread.sleep(SLEEP_TIME);
				this.progress.setValue((int)(1000. * this.thread.getProgression()));
			} 
			catch (InterruptedException e) 
			{
				// Ne rien faire
			}
		}
	
		this.thread.getPlugin().reset();
		
		// Il faut pr�venir qu'on a fini
		try {
			listener.computingWorkEnded();
		} catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<Couple<Integer, E>> getResults()
	{
		return this.thread.getResults();
	}
}
