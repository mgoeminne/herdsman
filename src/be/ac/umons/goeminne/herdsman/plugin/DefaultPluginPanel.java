package be.ac.umons.goeminne.herdsman.plugin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXDatePicker;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import be.ac.umons.goeminne.herdsman.core.DateRangeType;
import be.ac.umons.goeminne.herdsman.core.MySQLSession;
import be.ac.umons.goeminne.herdsman.gui.MainFrame;

public class DefaultPluginPanel<E> extends JPanel 
{
	private static final long serialVersionUID = 1L;
	
	private JProgressBar progress;
	private Plugin<E> p;
	private MainFrame frame;
	private JPanel drawPane;
	private ChartPanel cp;
	private JRadioButton bcal;
	private JSpinner sTime;
	private JComboBox uTime;
	private JCheckBox bCompletion;
	
	public DefaultPluginPanel(Plugin<E> p, MainFrame frame)
	{
		super();
		this.p = p;
		this.frame = frame;
		
		DefaultPluginListener pListener = new DefaultPluginListener(this);
		
		PluginActionListener<E> listener = new PluginActionListener<E>(this);
		setLayout(new BorderLayout());
		
		JPanel ret = new JPanel();
		ret.setLayout(new BoxLayout(ret, BoxLayout.Y_AXIS));
		
		JPanel pDate = new JPanel();
			pDate.setLayout(new FlowLayout(FlowLayout.LEFT));
			JCheckBox bDate = new JCheckBox("Plage de dates");
				bDate.setEnabled(false);
			pDate.add(bDate);
			JLabel lstart = new JLabel("D�but : ");
			JLabel lstop   = new JLabel("Fin : ");
			
			JXDatePicker start = new JXDatePicker();
			start.setEnabled(false);
			JXDatePicker stop  = new JXDatePicker();
			stop.setEnabled(false);
			
			pDate.add(lstart);
			pDate.add(start);
			pDate.add(lstop);
			pDate.add(stop);
			
			pDate.setBorder(new TitledBorder("Dates"));
			
		JPanel pDateSystem = new JPanel();
			pDateSystem.setLayout(new BoxLayout(pDateSystem, BoxLayout.Y_AXIS));
			
			JRadioButton bRev = new JRadioButton("revision");
				bRev.setSelected(true);
				bRev.setActionCommand("revisionMode");
				bRev.addActionListener(pListener);
			bcal = new JRadioButton("date");
				bcal.setActionCommand("dateMode");
				bcal.addActionListener(pListener);
				
			ButtonGroup bg = new ButtonGroup();
				bg.add(bRev);
				bg.add(bcal);
				
			
			JLabel lPlage = new JLabel("Unit� de temps : ");
			sTime = new JSpinner();
				sTime.setModel(new SpinnerNumberModel(1,1,100,1));
				sTime.setEnabled(false);
			uTime = new JComboBox();
				uTime.addItem(DateRangeType.DAY);
				uTime.addItem(DateRangeType.WEEK);	
				uTime.addItem(DateRangeType.MONTH);	
				uTime.addItem(DateRangeType.YEAR);
				uTime.setEnabled(false);
			
			bCompletion = new JCheckBox("Compl�ter les blancs");
				bCompletion.setEnabled(false);
			
			JPanel pDateChoice = new JPanel();
				pDateChoice.setLayout(new FlowLayout(FlowLayout.LEFT));
				pDateChoice.add(bRev);
				pDateChoice.add(bcal);
				pDateChoice.add(lPlage);
				pDateChoice.add(sTime);
				pDateChoice.add(uTime);
			
			JPanel pWhite = new JPanel();
				pWhite.setLayout(new FlowLayout(FlowLayout.LEFT));
				pWhite.add(bCompletion);
			
			pDateSystem.add(pDateChoice);
			pDateSystem.add(pWhite);
						
			
			pDateSystem.setBorder(new TitledBorder("Travailler par"));
			
		JPanel pCalcul = new JPanel();
			pCalcul.setLayout(new BorderLayout());
			JButton bCompute = new JButton("(re)calcul !");
				bCompute.addActionListener(listener);
				bCompute.setActionCommand("compute");
			progress = new JProgressBar(0, 1000);
			
			pCalcul.add(bCompute, BorderLayout.WEST);
			pCalcul.add(progress, BorderLayout.CENTER);
			pCalcul.setBorder(new TitledBorder("Calcul"));
			
		
		drawPane = new JPanel();
			
		ret.add(pDate);
		ret.add(pDateSystem);
		ret.add(pCalcul);
		
		this.add(ret, BorderLayout.NORTH);
		this.add(drawPane, BorderLayout.CENTER);
	}
	
	public final Plugin<E> getPlugin()
	{
		return p;
	}
	
	public final MySQLSession getSession()
	{
		return this.frame.getSession();
	}
	
	public final MainFrame getFrame()
	{
		return frame;
	}
	
	public final JProgressBar getProgressBar()
	{
		return this.progress;
	}
	
	public void setChart(JFreeChart chart)
	{
		drawPane.removeAll();
		cp = new ChartPanel(chart);
		drawPane.add(cp);
	
		this.frame.pack();
		this.frame.setVisible(true);
	}
	
	/**
	 * Determines if the user wants a date mode.
	 * @return	True if the user wants a date mode, false if the user wants a revision mode.
	 */
	public boolean getDateMode()
	{
		return bcal.isSelected();
	}
	
	public void setRevisionMode(boolean revision)
	{
		this.sTime.setEnabled(!revision);
		this.uTime.setEnabled(!revision);
		this.bCompletion.setEnabled(!revision);
	}
	
	public boolean isModeRevision()
	{
		return !this.bcal.isSelected();
	}
	
	public DateRangeType getRangeType()
	{
		return (DateRangeType) this.uTime.getSelectedItem();
	}
	
	public boolean mustFillHoles()
	{
		return bCompletion.isSelected();
	}
	
	public int getRangeSize()
	{
		return (Integer) sTime.getValue();
	}
}
