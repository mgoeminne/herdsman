package be.ac.umons.goeminne.herdsman.plugin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXDatePicker;
import org.jfree.chart.JFreeChart;
import org.jfree.data.time.DateRange;

import be.ac.umons.goeminne.herdsman.core.Couple;
import be.ac.umons.goeminne.herdsman.core.DateRangeType;
import be.ac.umons.goeminne.herdsman.core.MySQLSession;
import be.ac.umons.goeminne.herdsman.gui.MainFrame;

public abstract class Plugin<E>
{
	private double progression;		// Vaut 0 quand on n'a rien fait, 1 quand le travail est fini.
	private boolean finished; // Vaut true quand le travail est fini.
	private boolean work;		// Vaut true si on travaille.
	protected DefaultPluginPanel<E> panel;
	
	public Plugin()
	{
		progression = 0.;
		finished = false;
		work = false;
	}
	
	/**
	 * Donne l'avancement du travail.
	 * N'a pas d'effet si le travail a �t� d�clar� comme termin�.
	 * @param done	L'�tat d'avancement du travail.
	 */
	protected synchronized void setProgression(double done)
	{
		if(!finished)
		{
			this.progression = Math.min(1., Math.max(0., done));
		}		
	}
	
	/**
	 * D�clare que le travail est termin�.
	 */
	protected synchronized void finish()
	{
		this.finished = true;
		this.progression = 1.;
		this.work = false;
	}
	
	public synchronized boolean isFinished()
	{
		return this.finished;
	}
	
	public synchronized double getProgression()
	{
		return this.progression;
	}
	
	protected synchronized void start()
	{
		this.work = true;
		this.finished = false;
		this.progression = 0.;
	}
	
	/**
	 * Pr�pare le plugin � une nouvelle utilisation.
	 */
	public synchronized void reset()
	{
		this.work = false;
		this.finished = false;
		this.progression = 0.;
	}
	
	public synchronized boolean isWorking()
	{
		return this.work;
	}
	
	/**
	 * Calcule la m�trique pour une r�vision donn�e. S'il y a des d�pendances entre les r�visions, c'est au plugin
	 * de faire tous les calculs n�cessaires pour calculer ces d�pendances. Ce n'est typiquement pas le cas : g�n�ralement la valeur d'une m�trique pour 
	 * une r�vision r ne d�pend pas de la valeur pour la r�vision r-1.
	 * @param session	La session MySQL.
	 * @param rev_id	L'id de la r�vision � consid�rer.
	 * @return	La valeur de la m�trique pour cette r�vision.
	 * @throws SQLException
	 */
	public abstract E compute(MySQLSession session, int rev_id) throws SQLException;
	
	public ArrayList<Couple<Integer, E>> compute(MySQLSession session, int first, int last) throws SQLException
	{
		start();
		ArrayList<Couple<Integer, E>> ret = new ArrayList<Couple<Integer, E>>();
		
		// R�cup�ration des id int�ressants
		String query = 	"SELECT rev " + 
						"FROM scmlog " + 
						"WHERE rev BETWEEN " + first + " AND " + last + " " +
						"ORDER BY id ASC";
		
		Statement sc = session.getSCStatement();
		ResultSet repID = sc.executeQuery(query);
		
		while(repID.next())
		{
			int revision = repID.getInt("rev");
			E n = compute(session, revision);
			ret.add(new Couple<Integer, E>(revision, n));
			
			setProgression((double)revision / (double)(last - first));
		}
		
		sc.close();
		
		finish();
		return ret;
	}
	
	/**
	 * Calcule la la m�trique pour toutes les r�visions disponibles.
	 * @param session La session MySQL.
	 * @return	Une liste de couples (revision, m�trique) avec <i>revision</i> l'identifiant de la r�vision (par ordre croissant), <i>m�trique</i> la m�trique calcul�e.
	 * @throws SQLException 
	 */
	public ArrayList<Couple<Integer, E>> compute(MySQLSession session) throws SQLException
	{
		String query = 	"SELECT MIN(id) AS first " +
						"FROM scmlog ";

		Statement sc = session.getSCStatement();
		ResultSet rep = sc.executeQuery(query);
		rep.next();
		int first = rep.getInt("first");
		
		query = "SELECT MAX(id) AS last " +
				"FROM scmlog ";
		
		rep = sc.executeQuery(query);
		rep.next();
		int last = rep.getInt("last");
		
		return compute(session, first, last);
	}
	
	
	/**
	 * Donne un panneau qui repr�sente le plugin.
	 * @param frame La frame dans laquelle le panneau va s'int�grer.
	 * @return Le panneau par d�faut qui repr�sente le plugin.
	 */
	public JPanel getPanel(MainFrame frame)
	{
		if(panel == null || panel.getFrame() != frame)
			panel = new DefaultPluginPanel<E>(this, frame);
		return panel;
	}
	
	
	public abstract String toString();
	
	public String shortName()
	{
		return toString();
	}
	
	public abstract String getDescription();
	
	/**
	 * Gives a chart that represents the collected data, following user's preferences.
	 * @param session	The session.
	 * @param data		The collected data.
	 * @return	The chart representing the data.
	 */
	public abstract JFreeChart getChart(MySQLSession session, ArrayList<Couple<Integer, E>> data);
	
	/**
	 * Gives a chart that represents the collected data, following user's preferences.
	 * @param session	The session.
	 * @param data		The collected data.
	 * @return	The chart representing the data.
	 */
	public abstract JFreeChart getDateChart(MySQLSession session, ArrayList<Couple<DateRange, E>> values, DateRangeType rangeType);
}
