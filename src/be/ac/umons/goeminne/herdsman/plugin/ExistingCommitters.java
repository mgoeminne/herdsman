package be.ac.umons.goeminne.herdsman.plugin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import be.ac.umons.goeminne.herdsman.core.MySQLSession;

public class ExistingCommitters extends IntegerPlugin
{
	public ExistingCommitters()
	{
		super();
	}

	@Override
	public Integer compute(MySQLSession session, int revId) throws SQLException 
	{
		String query = 	"SELECT COUNT(DISTINCT l.committer_id) AS count " + 
						"FROM scmlog AS l " +
						"WHERE l.rev <= " + revId;
		
		Statement st = session.getSCStatement();
		ResultSet res = st.executeQuery(query);
		
		res.next();
		int ret = res.getInt("count");
		st.close();
		return ret;
	}
	

	@Override
	public String getDescription() 
	{
		return "Number of committers who have committed at least one time";
	}

	@Override
	public String toString() 
	{
		return "Existing committers";
	}
	
	public String shortName()
	{
		return "Committers";
	}
}
