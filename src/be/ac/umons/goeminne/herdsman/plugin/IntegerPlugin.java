package be.ac.umons.goeminne.herdsman.plugin;

import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.DateRange;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import be.ac.umons.goeminne.herdsman.core.Couple;
import be.ac.umons.goeminne.herdsman.core.DateRangeType;
import be.ac.umons.goeminne.herdsman.core.MySQLSession;

public abstract class IntegerPlugin extends Plugin<Integer> 
{
	public IntegerPlugin()
	{
		super();
	}
	
	@Override
	public JFreeChart getChart(MySQLSession session, ArrayList<Couple<Integer, Integer>> values) {
		XYSeries data = new XYSeries("Territoriality");
		XYSeriesCollection dataset = new XYSeriesCollection();
		
		for(Couple<Integer, Integer> val : values)
		{
			data.add(val.getFirst().doubleValue(), val.getSecond().doubleValue());
		}		
		
		dataset.addSeries(data);
		
		JFreeChart chart = ChartFactory.createXYLineChart(	this.panel.getFrame().getSession().getProject(), 
															"Revision", 
															this.panel.getPlugin().toString(), 
															dataset, 
															PlotOrientation.VERTICAL, 
															false,		// no legend
															true, 		// tooltips
															false);		// URL
		
		// Customisation de l'affichage
		XYPlot plot = (XYPlot) chart.getPlot();
		// change the auto tick unit selection to integer units only... 
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis(); 
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		
		NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
		domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		
		return chart;
	}

	public JFreeChart getDateChart(MySQLSession session, ArrayList<Couple<DateRange, Integer>> values, DateRangeType rangeType) {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		
		for(Couple<DateRange, Integer> val : values)
		{
			DateRange dr = val.getFirst();
			Integer value = val.getSecond();
			
			
			
			dataset.addValue(value, "terri", rangeType.getLabelName(dr));
		}		
		
		JFreeChart chart = ChartFactory.createLineChart(	this.panel.getFrame().getSession().getProject(), 
															"Period", 
															this.panel.getPlugin().toString(), 
															dataset, 
															PlotOrientation.VERTICAL, 
															false,						// No legend
															true,						// tooltips
															false);						// urls
		
		// Customisation de l'affichage
		CategoryPlot plot = chart.getCategoryPlot(); 
		//plot.setBackgroundPaint(Color.lightGray); 
		//plot.setDomainGridlinePaint(Color.white); 
		//plot.setRangeGridlinePaint(Color.white);
		
		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis(); 
		//rangeAxis.setUpperMargin(0.15);
		return chart;
	}
}
