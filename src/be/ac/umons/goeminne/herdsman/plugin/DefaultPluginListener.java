package be.ac.umons.goeminne.herdsman.plugin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DefaultPluginListener implements ActionListener 
{
	private DefaultPluginPanel panel;
	
	public DefaultPluginListener(DefaultPluginPanel p)
	{
		this.panel = p;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		String command = e.getActionCommand();
		
		if(command.equals("revisionMode"))
			this.panel.setRevisionMode(true);
		if(command.equals("dateMode"))
			this.panel.setRevisionMode(false);
	}
}
