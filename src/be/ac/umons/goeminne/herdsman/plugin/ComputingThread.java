package be.ac.umons.goeminne.herdsman.plugin;

import java.sql.SQLException;

import be.ac.umons.goeminne.herdsman.core.MySQLSession;
import be.ac.umons.goeminne.herdsman.core.Couple;
import java.util.*;

/**
 * Un thread pour r�aliser un calcul.
 * @author mathieugoeminne
 *
 */
public class ComputingThread<E> extends Thread 
{
	private Plugin<E> plugin;
	private boolean fullRange;		// True si on doit faire toute la port�e.
	private MySQLSession session;
	private int start, stop;
	private ArrayList<Couple<Integer, E>> ret;
	
	public ComputingThread(Plugin<E> plugin, MySQLSession session)
	{
		this.plugin = plugin;
		this.session = session;
		this.fullRange = true;
	}
	
	public ComputingThread(Plugin<E> plugin, MySQLSession session, int start, int stop)
	{
		this.plugin = plugin;
		this.session = session;
		this.fullRange = false;
		this.start = start;
		this.stop  = stop; 
	}
	
	public void run()
	{
		try 
		{
			if(this.fullRange)
				ret = this.plugin.compute(session);
			else
				ret = this.plugin.compute(session, this.start, this.stop);
		} 
		catch (SQLException e) 
		{
			// Un probl�me est survenu, on va dire qu'on n'obtient aucun r�sultat.
			ret = new ArrayList<Couple<Integer, E>>();
		}		
	}	
	
	public ArrayList<Couple<Integer, E>> getResults()
	{
		return ret;
	}
	
	public final boolean isFinished()
	{
		return this.plugin.isFinished();
	}
	
	public final double getProgression()
	{
		return this.plugin.getProgression();
	}
	
	public final Plugin<E> getPlugin()
	{
		return this.plugin;
	}
}
