package be.ac.umons.goeminne.herdsman.plugin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.*;

import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import be.ac.umons.goeminne.herdsman.core.*;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.DateRange;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class PluginActionListener<E> implements ActionListener, ComputeEventListener 
{
	private DefaultPluginPanel<E> panel;
	private ObserverComputing<E> observer;
	
	
	public PluginActionListener(DefaultPluginPanel<E> p)
	{
		this.panel = p;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		String command = e.getActionCommand();
		
		if(command.equals("compute"))
		{
			ComputingThread<E> comp = new ComputingThread<E>(this.panel.getPlugin(), this.panel.getSession());
			observer = new ObserverComputing<E>(this, this.panel.getProgressBar(), comp);
			
			observer.start();
		}
	}
	
	
	public void computingWorkEnded() throws SQLException
	{
		// Grabbing the results
		
		ArrayList<Couple<Integer, E>> ret = observer.getResults();
		if(!this.panel.isModeRevision())
		{
			SourceDateTranslator dt = new SourceDateTranslator(this.panel.getSession());
			ArrayList<Couple<DateRange, E>> retDate = dt.convertToDate(ret, this.panel.mustFillHoles(), this.panel.getRangeType(), this.panel.getRangeSize());
			
			// Put the chart in the panel
			this.panel.setChart(this.panel.getPlugin().getDateChart(this.panel.getSession(), retDate, this.panel.getRangeType()));
		}
		else
		{
			// Putting the chart in the panel
			this.panel.setChart(this.panel.getPlugin().getChart(this.panel.getSession(), ret));
		}	
	}
}
