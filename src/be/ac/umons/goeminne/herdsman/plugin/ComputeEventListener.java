package be.ac.umons.goeminne.herdsman.plugin;

import java.sql.SQLException;

/**
 * Ecoute les �v�nements de calcul
 * @author mathieugoeminne
 *
 */
public interface ComputeEventListener 
{
	/**
	 * Un �v�nement de calcul est termin�.
	 * @throws SQLException 
	 */
	public void computingWorkEnded() throws SQLException;
}
