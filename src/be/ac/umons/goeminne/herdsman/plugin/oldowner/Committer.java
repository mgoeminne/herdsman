package be.ac.umons.goeminne.herdsman.plugin.oldowner;

import java.util.*;

public class Committer 
{
	private int id;					// L'identifiant du commiter
	private int first_rev_id;		// L'identifiant de la premi�re r�vision o� il fait quelque chose
	private ArrayList<File> files;	// Les fichiers poss�d�s. Il suffit de le toucher en dernier pour le poss�der.
	
	/**
	 * 
	 * @param id		L'identifiant du committer.
	 * @param rev_id	La r�vision o� le committer � commenc� � s�vir.
	 */
	public Committer(int id, int rev_id)
	{
		this.id = id;
		this.first_rev_id = rev_id;
		files = new ArrayList<File>();
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public int getFirstRevisionID()
	{
		return this.first_rev_id;
	}
	
	/**
	 * Retourne le nombre de fichiers poss�d�s.
	 * @return	Le nombre de fichiers poss�d�s.
	 */
	public int getNbFiles()
	{
		return files.size();
	}
	
	/**
	 * Le committer acquiert un fichier.
	 * @param f	Le fichier qu'il acquiert.
	 */
	public void own(File f)
	{
		if(!files.contains(f))
			files.add(f);
	}
	
	/**
	 * Le committer perd un fichier.
	 * @param f	Le fichier perdu.
	 */
	public void lost(File f)
	{
		if(files.contains(f))
			files.remove(f);
	}
}
