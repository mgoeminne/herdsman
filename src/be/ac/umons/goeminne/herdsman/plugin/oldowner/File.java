package be.ac.umons.goeminne.herdsman.plugin.oldowner;

public class File 
{
	private int id;					// Son identifiant
	private Committer lastToucher;
	
	/**
	 * 
	 * @param id L'identifiant du fichier.
	 */ 
	public File(int id)
	{
		this.id = id;
		this.lastToucher = null;
	}
	
	public void touch(Committer c)
	{
		this.lastToucher = c;
	}
	
	public Committer getLastToucher()
	{
		return this.lastToucher;
	}
}
