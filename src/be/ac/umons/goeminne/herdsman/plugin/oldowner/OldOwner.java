package be.ac.umons.goeminne.herdsman.plugin.oldowner;

import java.sql.SQLException;
import java.util.*;

import be.ac.umons.goeminne.herdsman.plugin.Plugin;
import be.ac.umons.goeminne.herdsman.core.Couple;
import be.ac.umons.goeminne.herdsman.core.MySQLSession;

public class OldOwner extends Plugin<ArrayList<Couple<Integer, Integer>>> 
{
	public OldOwner()
	{
		super();		
	}
	
	public ArrayList<Couple<Integer, Integer>> compute(MySQLSession session, int revision_id) throws SQLException
	{
		ArrayList<Couple<Integer, ArrayList<Couple<Integer, Integer>>>> l = compute(session, 1, revision_id);
		assert( l.get(l.size()-1).getFirst() == revision_id);
		
		return l.get(l.size()-1).getSecond();	
	}
	
	/**
	 * Cette m�thode est surcharg�e car le calcul de cette m�trique d�pend du calcul de la m�trique pour les r�visions pr�c�dentes.
	 */
	public ArrayList<Couple<Integer, ArrayList<Couple<Integer, Integer>>>> compute(MySQLSession session, int first, int last) throws SQLException
	{
		return null;
	}	
}
