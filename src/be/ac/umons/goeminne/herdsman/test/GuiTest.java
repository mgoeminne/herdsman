package be.ac.umons.goeminne.herdsman.test;

import java.sql.SQLException;

import be.ac.umons.goeminne.herdsman.core.MySQLSession;
import be.ac.umons.goeminne.herdsman.gui.MainFrame;
import be.ac.umons.goeminne.herdsman.plugin.ExistingCommitters;
import be.ac.umons.goeminne.herdsman.plugin.territoriality.Territoriality;

public class GuiTest {

	public static void main(String[] args) throws SQLException 
	{
		MySQLSession session = new MySQLSession("root", "root", "Evince");
		session.setSourceCodeDB("evincefm");
		
		MainFrame mf = new MainFrame("Herdsman", session);
		
		mf.addPlugin(new Territoriality());
		mf.addPlugin(new ExistingCommitters());
		
		mf.pack();
		mf.setVisible(true);
	}

}
