package be.ac.umons.goeminne.herdsman.core;

public interface BugChangeListener 
{
	public void bugChanged();
}
