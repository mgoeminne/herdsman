package be.ac.umons.goeminne.herdsman.core;

public class Couple<A,B> 
{
	private A a;
	private B b;
	
	public Couple(A first, B second)
	{
		this.a = first;
		this.b = second;
	}
	
	public void setFirst(A first)
	{
		this.a = first;
	}
	
	public void setSecond(B second)
	{
		this.b = second;
	}
	
	public A getFirst()
	{
		return this.a;
	}
	
	public B getSecond()
	{
		return this.b;
	}
}
