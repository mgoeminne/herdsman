package be.ac.umons.goeminne.herdsman.core;

public interface SourceCodeChangeListener 
{
	public void sourceCodeChanged();
}
