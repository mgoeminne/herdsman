package be.ac.umons.goeminne.herdsman.core;

import java.sql.SQLException;
import java.util.ArrayList;

import org.jfree.data.time.DateRange;
import org.jfree.data.time.Day;

public interface DateTranslator {

	/**
	 * Gives the day associated to a revision.
	 * @param revisionID	The revision id.
	 * @return The day where the revision has been committed.
	 * @throws SQLException If a problem occurs during the bdd connexion.
	 */
	public abstract Day getDay(int revisionID) throws SQLException;

	/**
	 * Compute the action "under" a day.
	 * @param d	The day to considere.
	 * @return
	 * <ol>
	 * 		<li>If there is an action during <i>d</i>, return the first action this day.</li>
	 * 		<li>Else, return null</li>
	 * </ol>
	 * @throws SQLException
	 */
	public abstract Integer getFirstAction(Day d) throws SQLException;
	
	
	/**
	 * Compute the action "under" a day.
	 * @param d	The day to considere.
	 * @return
	 * <ol>
	 * 		<li>If there is an action during <i>d</i>, return the last action this day.</li>
	 * 		<li>Else, return null</li>
	 * </ol>
	 * @throws SQLException
	 */
	public abstract Integer getLastAction(Day d) throws SQLException;
	
	/**
	 * Computes the action "under" a day.
	 * @param d	The day to considere.
	 * @return The action id of the first action of this day. 
	 * <ol>
	 * 		<li>If there is an action during <i>d</i>, return the first action this day.</li>
	 * 		<li>Else, if there is an action after <i>d</i>, return the first action after this day.</li>
	 * 		<li>Else, return null.</li>
	 * </ol>
	 */
	public abstract Integer getFirstActionOrAfter(Day d) throws SQLException ;
	
	
	/**
	 * Computes the action "under" a day.
	 * @param d The day to considere.
	 * @return The action id of the last action of this day. 
	 * * <ol>
	 * 		<li>If there is an action during <i>d</i>, return the last action this day.</li>
	 * 		<li>Else, if there is an action before <i>d</i>, return the last action before this day.</li>
	 * 		<li>Else, return null.</li>
	 * </ol>
	 */
	public abstract Integer getLastActionOrBefore(Day d) throws SQLException;
	
	
	/**
	 * <p>
	 * Converts a list of metrics defined with an action id as temporal reference to a list of metrics with a date as temporal reference.
	 * If there are more than one metrics for a day, only the last metric of the day is taked into account. If there is no metric for a date, 
	 * either a null metric is putted in the list, or a metric identical to the previous one is putted in the list (depending on the completion policy).
	 * </p>
	 * <p>
	 * 
	 * <p>
	 * data are created as needed to have a partition of the range time, from the first action to the last.
	 * </p>
	 * @param <E>	The type of the considered metric.
	 * @param list	A list of action-dated metrics. We suppose the metrics are ordered by ascending action id.
	 * @param fillHole The completion policy. If true, a hole in the partition is filled with a metric wich is the same as the previous one. If false, the partition is filled with a null value.
	 * @param range The range of an element in the partition. For instance : WEEK, we considere a range of week(s).
	 * @param rangeSize The number of element to considere. For instance : 2 (with WEEK for the range), we considere a range of two weeks.
	 * @return	A list of day-dated and date-ordered metrics. There is a partition from the first metric to the last.
	 * @throws SQLException 
	 */
	public abstract <E> ArrayList<Couple<DateRange, E>> convertToDate(ArrayList<Couple<Integer, E>> list, boolean fillHole, DateRangeType range, int rangeSize) throws SQLException;
}