package be.ac.umons.goeminne.herdsman.core;

public interface MailChangeListener 
{
	public void mailChanged();
}
