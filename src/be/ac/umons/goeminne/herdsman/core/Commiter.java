package be.ac.umons.goeminne.herdsman.core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Commiter 
{
	private int id;
	private MySQLSession session;
	
	public Commiter(int id, MySQLSession session)
	{
		this.id = id;
		this.session = session;
	}
	
	public String getName()
	{
		try 
		{
			Statement stm = session.getSCStatement();
			String query = "SELECT name FROM people WHERE id='" + id + "'";
			ResultSet res = stm.executeQuery(query);
			return res.getString("name");
		} catch (SQLException e) 
		{
			return null;
		}
	}
	
	public String getMail()
	{
		try 
		{
			Statement stm = session.getSCStatement();
			String query = "SELECT email FROM people WHERE id='" + id + "'";
			ResultSet res = stm.executeQuery(query);
			return res.getString("email");
		} catch (SQLException e) 
		{
			return null;
		}
	}
}
