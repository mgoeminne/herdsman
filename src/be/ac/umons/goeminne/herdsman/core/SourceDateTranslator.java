package be.ac.umons.goeminne.herdsman.core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.jfree.data.time.Day;
import org.jfree.data.time.DateRange;


/**
 * Une classe capable de jouer avec les num�ros de r�vision et les dates associ�es.
 * @author mathieugoeminne
 */
public class SourceDateTranslator implements DateTranslator 
{
	private MySQLSession session;
	
	/**
	 * @param session La session utilis�e comme source de donn�es.
	 */
	public SourceDateTranslator(MySQLSession session)
	{
		this.session = session;
	}
	
	/* (non-Javadoc)
	 * @see be.ac.umons.goeminne.herdsman.core.DateTranslator#getDay(int)
	 */
	public Day getDay(int revisionID) throws SQLException
	{
		Statement st = session.getSCStatement();
		String query = 	"SELECT date " +
						"FROM scmlog " +
						"WHERE rev = " + revisionID + " " +
						"LIMIT 1";
		
		ResultSet results = st.executeQuery(query);
		results.next();
		Date date = results.getDate("date");
		
		st.close();
		
		return new Day(date);
	}
	
	/* (non-Javadoc)
	 * @see be.ac.umons.goeminne.herdsman.core.DateTranslator#getFirstAction(org.jfree.data.time.Day)
	 */
	public Integer getFirstAction(Day d) throws SQLException
	{
		if(isThereAnAction(d))
		{
			int day = d.getDayOfMonth();
			int month = d.getMonth();
			int year = d.getYear();
			
			String query = 	"SELECT rev " + 
			"FROM scmlog " + 
			"WHERE date BETWEEN \"" + year + "-" + month + "-" + day + " 0:0:0\" AND \"" + year + "-" + month + "-" + day + " 23:59:59\" " +
			"ORDER BY rev " +
			"LIMIT 1";
			Statement st = session.getSCStatement();
			ResultSet res = st.executeQuery(query);
			res.next();
			Integer ret = res.getInt("rev");
			st.close();
			return ret;	
		}
		else
			return null;
	}
	
	/* (non-Javadoc)
	 * @see be.ac.umons.goeminne.herdsman.core.DateTranslator#getLastAction(org.jfree.data.time.Day)
	 */
	public Integer getLastAction(Day d) throws SQLException
	{
		if(isThereAnAction(d))
		{
			int day = d.getDayOfMonth();
			int month = d.getMonth();
			int year = d.getYear();
			
			String query = 	"SELECT rev " + 
			"FROM scmlog " + 
			"WHERE date BETWEEN \"" + year + "-" + month + "-" + day + " 0:0:0\" AND \"" + year + "-" + month + "-" + day + " 23:59:59\" " +
			"ORDER BY rev DESC" +
			"LIMIT 1";
			Statement st = session.getSCStatement();
			ResultSet res = st.executeQuery(query);
			res.next();
			Integer ret = res.getInt("rev");
			st.close();
			return ret;
		}
		else
			return null;
	}
	
	/**
	 * Determines if there is at least one revision during a given day.
	 * @param d	The day to considere.
	 * @return	True if there is at least one revision during <i>d</i>.
	 * @throws SQLException
	 */
	private boolean isThereAnAction(Day d) throws SQLException
	{
		int day = d.getDayOfMonth();
		int month = d.getMonth();
		int year = d.getYear();
		
		String queryCount = 	"SELECT COUNT(*) AS count " + 
								"FROM scmlog " +
								"WHERE date BETWEEN \"" + year + "-" + month + "-" + day + " 0:0:0\" AND " +
									"\"" + year + "-" + month + "-" + day + " 23:59:59\"";
		
		Statement st = session.getSCStatement();
		ResultSet res = st.executeQuery(queryCount);
		res.next();
		int count = res.getInt("count");
		st.close();
		
		return count > 0;
	}

	@Override
	public Integer getFirstActionOrAfter(Day d) throws SQLException 
	{
		Integer ret = getFirstAction(d);
		if(ret != null)
			return ret;
		else
			return getActionAfter(d);
	}

	

	@Override
	public Integer getLastActionOrBefore(Day d) throws SQLException 
	{
		Integer ret = getLastAction(d);
		if(ret != null)
			return ret;
		else
			return getActionBefore(d);
	}
	
	/**
	 * 
	 * @param d A day to considere.
	 * @return the id of the first action after <i>d</i>, null if any.
	 * @throws SQLException 
	 */
	private Integer getActionAfter(Day d) throws SQLException 
	{
		int day = d.getDayOfMonth();
		int month = d.getMonth();
		int year = d.getYear();
		
		String query = 	"SELECT rev " +
						"FROM scmlog " +
						"WHERE date > \"" + year + "-" + month + "-" + day + " 23:59:59\"" + 
						"ORDER BY date " +
						"LIMIT 1";
		
		Statement st = session.getSCStatement();
		ResultSet res = st.executeQuery(query);
		
		if(res.next())
		{
			int rev = res.getInt("rev");
			st.close();
			return rev;
		}
		else
		{
			st.close();
			return null;
		}		
	}
	
	/**
	 * Gives the last revision before the date.
	 * @param d	The date to considere.
	 * @return	The revision id of the last revision before <i>d</i>. null if any.
	 * @throws SQLException
	 */
	private Integer getActionBefore(Day d) throws SQLException
	{
		int day = d.getDayOfMonth();
		int month = d.getMonth();
		int year = d.getYear();
		
		String query = 	"SELECT rev " +
						"FROM scmlog " +
						"WHERE date < \"" + year + "-" + month + "-" + day + " 00:00:00\"" + 
						"ORDER BY date DESC " +
						"LIMIT 1";
		
		Statement st = session.getSCStatement();
		ResultSet res = st.executeQuery(query);
		
		if(res.next())
		{
			int rev = res.getInt("rev");
			st.close();
			return rev;
		}
		else
		{
			st.close();
			return null;
		}		
	}
	
	
	public <E> ArrayList<Couple<DateRange, E>> convertToDate(ArrayList<Couple<Integer, E>> list, boolean fillHole, DateRangeType range, int rangeSize) throws SQLException
	{
		ArrayList<Couple<DateRange, E>> ret = new ArrayList<Couple<DateRange, E>>();
				
		Day firstDay = this.getDay(list.get(0).getFirst());
		Day lastDay = range.add(firstDay, rangeSize-1);
		
		Date d = range.getStart(firstDay);
		Date f = range.getEnd(lastDay);
		
		E lastVal = null;
		
		for(Couple<Integer, E> c : list)
		{
			int revision = c.getFirst();
			Day revisionDay = this.getDay(revision);
			E val = c.getSecond();
			
			if(revisionDay.getEnd().before(f) || revisionDay.getEnd().equals(f))		// We are always in the current range
			{
				lastVal = val;
			}
			else	// We have exceeded the current range
			{
				ret.add(new Couple<DateRange, E>(new DateRange(d,f), lastVal));
				lastVal = val;
								
				boolean found = false;	// True when we found a non empty date range
				
				// We need to cross over all potential empty date ranges
				do
				{
					firstDay = range.add(firstDay, rangeSize);
					lastDay  = range.add(lastDay, rangeSize);
					d = range.getStart(firstDay);
					f = range.getEnd(lastDay);
				
					if(revisionDay.compareTo(lastDay) > 0)	// an empty date ranges
						ret.add(new Couple<DateRange, E>(new DateRange(d,f), lastVal));
					else
						found = true;
				}
				while(! found);
			}
		}
		
		return ret;
	}
}
