package be.ac.umons.goeminne.herdsman.core;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class MySQLSession 
{
	private String host;
	private int port;
	private String user;
	private String password;
	private String project;		// Le nom du projet
	
	private Connection connSC, connBug, connMail;
	private String sc, mail, bug;
	
	private ArrayList<SourceCodeChangeListener> sourceCodeListeners;
	private ArrayList<MailChangeListener> mailListeners;
	private ArrayList<BugChangeListener> bugListeners;
	
	public MySQLSession(String host, int port, String user, String password, String project)
	{
		this.host = host;
		this.port = port;
		this.user = user;
		this.password = password;	
		this.project = project;
		
		sourceCodeListeners = new ArrayList<SourceCodeChangeListener>();
		mailListeners 		= new ArrayList<MailChangeListener>();
		bugListeners 		= new ArrayList<BugChangeListener>();
	}
	
	public MySQLSession(String project)
	{
		this("localhost", 8889, "root", "toor", project);
		this.host = "localhost";
		this.user = "root";
		this.password = "toor"; 
	}
	
	/**
	 * Cr�e une connexion � l'h�te local, avec un compte utilisateur et un mot de passe particuliers.
	 * @param user		Le compte utilisateur.
	 * @param password	Le mot de passe.
	 */
	public MySQLSession(String user, String password, String project)
	{
		this(project);
		this.user = user;
		this.password = password;
	}
	
	public void setSourceCodeDB(String name) throws SQLException
	{
		try
		{
			if(!(connSC == null))
			{
				connSC.commit();
				connSC.close();
			}
			
			System.out.println("jdbc:mysql://" + this.host + ":" + this.port + "/" + name);
			connSC = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + name, this.user, this.password); 
			sc = name;		
			for(SourceCodeChangeListener l : sourceCodeListeners)
				l.sourceCodeChanged();			
		}
		catch(SQLException e)
		{
			connSC = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + sc + "/"); 
		}
	}
	
	public void setMailDB(String name) throws SQLException
	{
		try
		{
			if(!(connMail == null))
			{
				connMail.commit();
				connMail.close();
			}
			
			connMail = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + name + "/"); 
			mail = name;
			for(MailChangeListener l : mailListeners)
				l.mailChanged();			
		}
		catch(SQLException e)
		{
			connMail = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + mail + "/"); 
		}
	}
	
	public void setBugDB(String name) throws SQLException
	{
		try
		{
			if(!(connBug == null))
			{
				connBug.commit();
				connBug.close();
			}
			
			connBug = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + name + "/");
			bug = name;
			for(BugChangeListener l : bugListeners)
				l.bugChanged();
		}
		catch(SQLException e)
		{
			connBug = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + bug + "/");
		}
		
	}
	
	public Statement getSCStatement() throws SQLException
	{
		return connSC.createStatement();
	}
	
	public Statement getBugStatement() throws SQLException
	{
		return connBug.createStatement();
	}
	
	public Statement getMailStatement() throws SQLException
	{
		return connMail.createStatement();
	}
	
	public void setUser(String user) throws SQLException
	{
		this.user = user;
		if(sc != null)
			setSourceCodeDB(sc);
	}
	
	public void setPassword(String password) throws SQLException
	{
		this.password = password;
		if(sc != null)
			setSourceCodeDB(sc);
	}
	
	public void addChangeListener(SourceCodeChangeListener l)
	{
		if(! sourceCodeListeners.contains(l))
			sourceCodeListeners.add(l);
	}
	
	public void addChangeListener(MailChangeListener l)
	{
		if(! mailListeners.contains(l))
			mailListeners.add(l);
	}
	
	public void addChangeListener(BugChangeListener l)
	{
		if(! bugListeners.contains(l))
			bugListeners.add(l);
	}
	
	public void removeChangeListener(SourceCodeChangeListener l)
	{
		if(sourceCodeListeners.contains(l))
			sourceCodeListeners.remove(l);
	}
	
	public void removeChangeListener(MailChangeListener l)
	{
		if(mailListeners.contains(l))
			mailListeners.remove(l);
	}
	
	public void removeChangeListener(BugChangeListener l)
	{
		if(bugListeners.contains(l))
			bugListeners.remove(l);
	}
	
	public void close() throws SQLException
	{
		if(connSC != null)
			connSC.close();
		if(connBug != null)
			connBug.close();
		if(connMail != null)
			connMail.close();
	}
	
	public void setProject(String project)
	{
		this.project = project;
	}
	
	public String getProject()
	{
		return this.project;
	}
}
