package be.ac.umons.goeminne.herdsman.core;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.jfree.data.time.*;

public enum DateRangeType 
{
	DAY,
	WEEK,
	MONTH,
	YEAR;
	
	/**
	 * Gives the day in which the date range containing the given day begins.
	 * @param d	A day.
	 * @return	The first day of the day/week/month/year containing <i>d</i>.
	 */
	public Date getStart(Day d)
	{
		switch(this)
		{
			case DAY:
				return d.getStart();
			case WEEK:
				Date date = new Date(d.getFirstMillisecond()); 
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(date);
				
				int week = cal.get(Calendar.WEEK_OF_YEAR);
				
				while(cal.get(Calendar.WEEK_OF_YEAR) == week)
				{
					cal.add(Calendar.DAY_OF_MONTH, -1);
				}
				cal.add(Calendar.DAY_OF_MONTH, 1);
				cal.set(Calendar.HOUR, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				
				return cal.getTime();
				
			case MONTH:
				Month m = new Month(d.getMonth(), d.getYear());
				return m.getStart();
			case YEAR:
				Year y = new Year(d.getYear());
				return y.getStart();
			default:
				return d.getStart();
		}
	}
	
	/**
	 * Gives the day in which the date range containing the given day ends.
	 * @param d A day.
	 * @return The last day of the day/week/month/year containing <i>d</i>.
	 */
	public Date getEnd(Day d)
	{
		switch(this)
		{
			case DAY:
				return d.getEnd();
			case WEEK:
				Date date = new Date(d.getFirstMillisecond()); 
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(date);
				
				int week = cal.get(Calendar.WEEK_OF_YEAR);
				
				while(cal.get(Calendar.WEEK_OF_YEAR) == week)
				{
					cal.add(Calendar.DAY_OF_MONTH, 1);
				}
				cal.add(Calendar.DAY_OF_MONTH, -1);
				cal.set(Calendar.HOUR, 23);
				cal.set(Calendar.MINUTE, 59);
				cal.set(Calendar.SECOND, 59);
				cal.set(Calendar.MILLISECOND, 999);
				
				return cal.getTime();
				
			case MONTH:
				Month m = new Month(d.getMonth(), d.getYear());
				return m.getEnd();
			case YEAR:
				Year y = new Year(d.getYear());
				return y.getEnd();
			default:
				return d.getEnd();
		}
	}
	
	public String toString()
	{
		switch(this)
		{
			case DAY : return "day(s)"; 
			case WEEK : return "week(s)"; 
			case MONTH : return "month(s)"; 
			case YEAR : return "year(s)"; 
			default : return "UNKNOW"; 
		}
	}
	
	/**
	 * Gives a day, based on an other day, and an increasing.
	 * @param d	The reference day.
	 * @param number	The number of day/week/month/year.
	 * @return	The new day.
	 */
	public Day add(Day d, int number)
	{
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date(d.getMiddleMillisecond()));
		
		switch(this)
		{
			case DAY:
				cal.add(Calendar.DAY_OF_MONTH, number);
				break;
			case WEEK:
				cal.add(Calendar.DAY_OF_MONTH, number*7);
				break;
			case MONTH:
				cal.add(Calendar.MONTH, number);
				break;
			case YEAR:
				cal.add(Calendar.YEAR, number);
				break;
			default:
				// DO NOTHING
		}
		
		return new Day(cal.getTime());
	}
	
	/**
	 * Gives a name for a label, depending of the date range.
	 * @param dr	The date range to considere.
	 * @return A name for a label.
	 */
	public String getLabelName(DateRange dr)
	{
		Day first = new Day(dr.getLowerDate());
		Day last  = new Day(dr.getUpperDate());
		
		switch(this)
		{
			case DAY:
				if(first.equals(last))
					return twoDigits(first.getDayOfMonth()) + "/" + twoDigits(first.getMonth()) + "/" + twoDigits(first.getYear());
				else
					return twoDigits(first.getDayOfMonth()) + "/" + twoDigits(first.getMonth()) + "/" + twoDigits(first.getYear()) + " - " + twoDigits(last.getDayOfMonth()) + "/" + twoDigits(last.getMonth()) + "/" + twoDigits(last.getYear());
			
			case WEEK:
				GregorianCalendar start = new GregorianCalendar();
					start.setTime(first.getStart());
				GregorianCalendar stop = new GregorianCalendar();
					stop.setTime(last.getStart());
					
				int startWeek = start.get(Calendar.WEEK_OF_YEAR);
				int stopWeek  = stop.get(Calendar.WEEK_OF_YEAR);
				
				if((startWeek == stopWeek) && (start.get(Calendar.YEAR) == stop.get(Calendar.YEAR)))
					return twoDigits(startWeek) + "-" + twoDigits(first.getYear());
				else
					return twoDigits(startWeek) + "-" + twoDigits(first.getYear()) + " - " + twoDigits(stopWeek) + "-" + twoDigits(last.getYear());
				
			case MONTH:
				if((first.getMonth() == last.getMonth()) && (first.getYear() == last.getYear()))
					return twoDigits(first.getMonth()) + "/" + twoDigits(first.getYear());
				else
					return twoDigits(first.getMonth()) + "/" + twoDigits(first.getYear()) + " - " + twoDigits(last.getMonth()) + "/" + twoDigits(last.getYear());
					
			case YEAR:
				if(first.getYear() == last.getYear())
					return new Integer(first.getYear()).toString();
				else
					return twoDigits(first.getYear()) + " - " + twoDigits(last.getYear());
			default: return "UNKNOW";
		}
	}
	
	
	/**
	 * Gives a string representation of a number with two digits. If necessary, a 0 is added in head.
	 * @param val	The positive or nul value to represent.
	 * @return A two-digits string representation of <i>val</i>.
	 */
	private final String twoDigits(Integer val)
	{
		if(val<10)
			return "0" + val;
		else
		{
			String str = val.toString();
			return str.substring(str.length()-2);
		}
	}
}
